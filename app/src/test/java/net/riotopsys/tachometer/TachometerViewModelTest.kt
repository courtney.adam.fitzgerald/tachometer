package net.riotopsys.tachometer

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import junit.framework.TestCase.assertEquals
import org.junit.Rule
import org.junit.Test
//import org.mockito.Mockito

//inline fun <reified T> mock(): T = Mockito.mock(T::class.java)

class TachometerViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Test
    fun testTack() {

        val dut = TachometerViewModel()

        assertEquals(0, dut.tack())

        dut.onEvent(2000)
        dut.onEvent(4000)
        dut.onEvent(6000)
        dut.onEvent(7000)
        dut.onEvent(9000)

        assertEquals(1750, dut.tack())

    }

    @Test
    fun testStddev() {
        val dut = TachometerViewModel()

        assertEquals(-0.0, dut.stddev())

        dut.onEvent(2000)
        dut.onEvent(4000)
        dut.onEvent(6000)
        dut.onEvent(7000)
        dut.onEvent(9000)

        assertEquals(500.0, dut.stddev())
    }

    @Test
    fun testIntervals() {
        val dut = TachometerViewModel()

        assertEquals(emptyList<Long>(), dut.intervals())

        dut.onEvent(2000)
        dut.onEvent(4000)
        dut.onEvent(6000)
        dut.onEvent(7000)
        dut.onEvent(9000)

        val intervals = dut.intervals()

        assertEquals( 4, intervals.size )
        assertEquals( 2000, intervals[0] )
        assertEquals( 2000, intervals[1] )
        assertEquals( 1000, intervals[2] )
        assertEquals( 2000, intervals[3] )

    }
}