package net.riotopsys.tachometer

import android.os.Bundle
import android.os.SystemClock
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.riotopsys.tachometer.ui.theme.TachometerTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import java.text.DecimalFormat
import kotlin.math.sqrt

import android.media.SoundPool
import android.media.AudioAttributes
import android.util.Log

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel: TachometerViewModel by viewModels()

        setContent {
            TachometerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    TachometerScreen(viewModel)
                }
            }
        }

        var soundPool: SoundPool = SoundPool.Builder()
            .setMaxStreams(1)
            .setAudioAttributes(
                AudioAttributes
                    .Builder()
                    .setUsage(
                        AudioAttributes.USAGE_ALARM
                    ).setContentType(
                        AudioAttributes.CONTENT_TYPE_SONIFICATION
                    ).build()
            ).build()!!

        var bell =  soundPool.load( this, R.raw.harvey, 1 )

        viewModel.setAudio( soundPool, bell )

    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TachometerTheme {
        Greeting("Android")
    }
}

@Composable
fun TachometerScreen(viewModel: TachometerViewModel) {

    val currentState: List<Long> by viewModel.events.observeAsState(initial = emptyList())

    val dec = DecimalFormat("#,###.##")

    Column(modifier = Modifier
        .fillMaxSize()
        .padding(8.dp)) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(.25f),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically) {
            if ( currentState.isEmpty()){
                Text(text = "no data")
            } else {
                Text(
                    text = "Tack: ${dec.format(viewModel.tack()/1000.0)} sec \n" +
                        "Stddev: ${dec.format(viewModel.stddev() /1000)} sec \n" +
                        "count: ${viewModel.events.value?.size} \n")
            }
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(.5f)
                .padding(8.dp),
            onClick = {
                viewModel.onEvent(SystemClock.elapsedRealtime())
            }) {
            Text(text = "push on event")
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(.25f)
                .padding(8.dp),
            onClick = viewModel::onReset) {
            Text(text = "Reset")
        }
    }

}

@Preview(showBackground = true)
@Composable
fun TachometerScreenPreview() {
    TachometerScreen(TachometerViewModel())
}

class TachometerViewModel(): ViewModel(){

    private var bell = 0
    private lateinit var soundPool: SoundPool

    private val _events = MutableLiveData<List<Long>>(emptyList())
    val events = _events as LiveData<List<Long>>

    fun onEvent( time: Long ){
        val result = _events.value?.let {
            it.plus(time)
        } ?: listOf(time)

        if ( result.size % 10 == 0 ){
            soundPool.play(bell, 1f, 1f, 1, 0, 0f)
        }
        _events.value = result

    }

    fun onReset(){
        _events.value = emptyList()
    }

    fun tack(): Long {
        val intervals = intervals()
        return if ( intervals.isEmpty() ) {
            0
        } else {
            intervals.sum() / intervals.size
        }
    }

    fun stddev(): Double {
        val mean = tack().toDouble()
        val intervals = intervals()
        return sqrt(intervals.map { (it - mean)*(it - mean) }.sum() / (intervals.size -1).toDouble())
    }

    fun intervals() = _events.value?.let { list ->
        list.mapIndexedNotNull { index, time ->
            if (index + 1 >= list.size) {
                null
            } else {
                list[index + 1] - time
            }
        }
    } ?: emptyList()

    fun setAudio(soundPool: SoundPool, bell: Int) {
        this.soundPool = soundPool
        this.bell = bell
    }


}